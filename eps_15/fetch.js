const fetch = require('node-fetch');

let promesa = fetch('https://api.github.com/users/williamromero');
promesa.then((res)=>{
  return res.json();
}).then((json) =>{
  console.log(json.login);
})

let prom = fetch('https://www.webres-studio.com/api/v1/posts/5');
prom.then((res)=>{
  return res.json();
}).then((json) =>{
  console.log(json.data.title);
})
