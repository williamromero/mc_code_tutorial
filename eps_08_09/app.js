const _ = require('lodash')

let comando = process.argv[2];

let x = { "nombre" : "William"}
let y = { "apodo" : "Chilli_Willy"}
let z = [ 
          { nombre:"William", apellido:"Romero", edad:"30"  }, 
          { nombre:"Lucia", apellido:"Castro", edad:"31"  } 
        ]
let resultado = _.assign(x,y);
console.log(resultado);
console.log(`Lodash Assign Function: ${resultado.nombre}`);
console.log(`Lodash Assign Function: ${resultado.apodo}`);

let res_two = _.find(z, {nombre: "Lucia"});
console.log(res_two);
console.log(`Lodash Find Function: ${res_two.nombre} ${res_two.apellido} | Edad: ${res_two.edad}`)

console.log(`\n//////// USANDO ARGV PARA TOMAR INFO DATA DEL USUARIO EN CONSOLA ////////\n`)

if (comando === 'usuario' ){
  if (process.argv[3] === 'William') {
    console.log(_.assign(x,y))
  } else {
    console.log('Usuario no encontrado')
  }
}

