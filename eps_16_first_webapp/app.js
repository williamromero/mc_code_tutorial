const express = require('express');
const app = express();

let isLogin = () => false;

let logger = (req, res, next) => {
  console.log('REQ TYPE: ', req.method);
  next();
}

let showIP = (req, res, next) => {
  console.log('IP: ', req.headers['x-forwarded-for'] || req.connection.remoteAddress);
  next();
}

app.use((req, res, next)=>{
  if(isLogin()){
    next();
  } else {
    res.send('User is logged out')
  }
}, logger, showIP)

app.use(logger);

app.get('/:user', (req, res) => {
  let usuario = req.params.user;
  res.send(`Hello ${req.method} ${usuario}`)
})

app.post('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.put('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.delete('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.listen(3000, ()=> {
  console.log('Example app listening on port 3000!')
})
