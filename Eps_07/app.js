const fs = require('fs')

// RESPUESTA ASINCRÓNICA
console.log('Iniciando')
fs.readFile('data.txt', 'utf-8', (error, data) => {
  if(error){
    console.log(`Error ${error}`)
  } else {
    console.log(data)
  }
})
console.log('Finalizado')
// RESPUESTA ASINCRÓNICA

// RESPUESTA
  // Iniciando
  // Finalizando
  // ¡Prueba de contenido sincrónico/asincrónico!
// RESPUESTA

// RESPUESTA SINCRÓNICA
console.log('Iniciando')
let data = fs.readFileSync('data.txt', 'utf-8');
console.log(data);
console.log('Finalizado')
// RESPUESTA SINCRÓNICA

// RESPUESTA
  // Iniciando
  // ¡Prueba de contenido sincrónico/asincrónico!
  // Finalizando
// RESPUESTA

fs.rename('data.txt', 'data_renamed.txt', (error) => {
  if(error) throw error;
  console.log('Renombrado');
})

fs.appendFile('data.txt', '\n Texto incluido con el método appendFile del módulo FS', (error) =>{
  if (error) console.log(`Error ${error}`)
})

fs.unlink('data2.txt', (error) =>{
  if(error) throw error;
  console.log('Eliminado')
})

fs.createReadStream('data.txt').pipe(fs.createWriteStream('data_new.txt'));

fs.readdir('../', (error, files) => {
  console.log('Root Folder \n')
  files.forEach(file => {
    if(file !== '.git' && file !== '.DS_Store' ) {
      console.log(file);
    }
  });
});

// app.js
// data.txt
// data2.txt
// data_new.txt
