var express = require('express');
var router = express.Router();

/* GET home page. */
let personas = [
  { id: 1, nombre: "William Romero" },
  { id: 2, nombre: "Oswaldo Vela" },
  { id: 3, nombre: "Luis Vega" }
]

router.get('/', (req, res) => {
  res.render('index', {
    titulo: 'Pug Templating Engine',
    mensaje: 'Running Node/Express Webapp', 
    personas}
  )
})

module.exports = router;
