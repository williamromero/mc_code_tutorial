const os = require('os');
const fs = require('fs');
const resp = require('./mc_require');

let cpu = os.cpus();
let os_cpu = os.platform();
let usuario = os.hostname();
let cpu_string = JSON.stringify(cpu);

fs.appendFile('mc_code.txt', `Información de CPU: + ${cpu_string}`, function(error){
  if(error){
    console.log('Error al crear archivo');
  }
});

resp.saludar();
console.log(`Respuesta de número de Clientes: ${resp.clients}`);
console.log(`Respuesta de Función Suma: ${resp.sumar(4,5)}`);
console.log(`Respuesta de Función Mostrar: ${resp.mostrar(10)}`);

console.log(cpu);
console.log(`Sistema Operativo: ${os_cpu}`);
console.log(`Usuario: ${usuario}`);

setTimeout(() => {
  console.log('Tareas finalizadas')
}, 2000)
