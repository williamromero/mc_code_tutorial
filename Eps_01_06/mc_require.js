// module.exports.saludar = function () {
//   console.log('[::] Respuesta Consola: Función de respuesta de módulo exportado [::]');
// };

let clientes = 200;
// module.exports.clients = clientes;

// console.log(module);
module.exports = {
  clients: clientes,
  saludar: () => { console.log('Respuesta de array "Exports" con función anónima') },
  sumar: (a, b) => a + b,
  mostrar: (param) => param + 10
}
