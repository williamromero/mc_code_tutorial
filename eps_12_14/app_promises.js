let prom = require('./promises');
prom.calcular(1, 6).then((resultado) => {
  console.log(`Resultado: ${resultado}`);
}, (error)=> {
  console.log(error);
})

// let promesa = new Promise((res, rej) => {
//   res('Éxito al procesar datos');
//   // rej('Error');
// })

// promesa.then((resultado)=>{
//   console.log(resultado)
// }, (error) =>{
//   console.log(error)
// })
