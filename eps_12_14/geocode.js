const request = require('request');
const argv = require('yargs').argv;

let direccion = argv.direccion;
let url = `http://maps.googleapis.com/maps/api/geocode/json?address=${direccion}`

request({
  url: url,
  json: true
}, function (error, response, body){
  if(error) { console.log('Servicio no disponible')
  } else if (body.status == 'ZERO RESULTS'){ console.log('No hay datos que mostrar')
  } else if (body.status == 'OVER_QUERY_LIMIT'){ console.log('Límite excedido de consultas')
  } else if (body.status == 'OK'){  
    console.log(JSON.stringify(body, undefined, 2));
    console.log(JSON.stringify(body.results[0].formatted_address))
    console.log(JSON.stringify(body.results[0].address_components[1].long_name))
    console.log(JSON.stringify('Lat: '+body.results[0].geometry.location.lat))
    console.log(JSON.stringify('Lng: '+body.results[0].geometry.location.lng))  
  }
});
