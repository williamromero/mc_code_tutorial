## NODE JS

NodeJS utiliza módulos para completar algunas tareas básicas de código.
<pre>
  const os = require('os');
  const fs = require('fs');
  let cpu = os.cpus();
  let os_cpu = os.platform();
  let usuario = os.hostname();
  let cpu_string = JSON.stringify(cpu);
  fs.appendFile('mc_code.txt', `Información de CPU: + ${cpu_string}`, function(error){
    if(error){
      console.log('Error al crear archivo');
    }
  });
  <b>node app.js</b>
  [
    { model: 'Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz', speed: 3300,
      times: { user: 1871240, nice: 0, sys: 1077170, idle: 40695620, irq: 0 } },
    { model: 'Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz', speed: 3300,
      times: { user: 1419030, nice: 0, sys: 543970, idle: 41680600, irq: 0 } },
    { model: 'Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz', speed: 3300,
      times: { user: 1404000, nice: 0, sys: 533450, idle: 41706160, irq: 0 } },
    { model: 'Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz', speed: 3300,
      times: { user: 1398420, nice: 0, sys: 532570, idle: 41712620, irq: 0 } } 
  ]
</pre>

### REQUIRE MODULES
Para requerir archivos o módulos producidos por nosotros.
**Module.exports** es un array de exportaciones de elementos de código. Es básicamente el que crea la lista de dependencias que deberán de ser requeridas para la correcta ejecución de todos los archivos.

<pre>
  <b>app.js</b>
  const os = require('os');
  const fs = require('fs');
  const ex_req = require('./mc_require');

  let cpu = os.cpus();
  let os_cpu = os.platform();
  let usuario = os.hostname();

  let cpu_string = JSON.stringify(cpu);

  ex_req.saludar();

  fs.appendFile('mc_code.txt', `Información de CPU: + ${cpu_string}`, function(error){
    if(error){
      console.log('Error al crear archivo');
    }
  });

  <b>mc_require.js</b>
  module.exports.saludar = function () {
    console.log('[::] Respuesta Consola: Función de respuesta de módulo exportado [::]');
  }

  <b>Repuesta: mc_code.txt</b>
  Información de CPU: + [{"model":"Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz","speed":3300,"times":{"user":2600650,"nice":0,"sys":1517230,"idle":49712770,"irq":0}},{"model":"Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz","speed":3300,"times":{"user":2063590,"nice":0,"sys":782520,"idle":50984110,"irq":0}},{"model":"Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz","speed":3300,"times":{"user":2042350,"nice":0,"sys":767460,"idle":51020410,"irq":0}},{"model":"Intel(R) Core(TM) i5-6600 CPU @ 3.30GHz","speed":3300,"times":{"user":2036190,"nice":0,"sys":766910,"idle":51027130,"irq":0}}]

  <b>Repuesta:</b>
  [::] Respuesta Consola: Función de respuesta de módulo exportado [::]
</pre>

### File System

Para tomar archivos, se pueden utilizar funciones del módulo **FS** que viene interno en el paquete de Node.

<pre>
  <b>Carga Asincrónica</b>

  console.log('Iniciando')
  fs.readFile('data.txt', 'utf-8', (error, data) => {
    if(error){
      console.log(`Error ${error}`)
    } else {
      console.log(data)
    }
  })
  console.log('Finalizado')
  <b>
  RESPUESTA
    Iniciando
    Finalizando
    ¡Prueba de contenido sincrónico/asincrónico!
  RESPUESTA
  </b>
  <b>Carga Sincrónica</b>

  console.log('Iniciando')
  let data = fs.readFileSync('data.txt', 'utf-8');
  console.log(data);
  console.log('Finalizado')
  <b>
  RESPUESTA
    Iniciando
    ¡Prueba de contenido sincrónico/asincrónico!
    Finalizando
  RESPUESTA
  </b>
</pre>

### Manipulación de Archivos con el Módulo FS
Each method has the **Sync** sufix to load changes synchronously.
Ex. renameSync, appendFileSync, unlinkSync.

<pre>
  <b>Renombrar archivo</b>
  fs.rename('data.txt', 'data_renamed.txt', (error) => {
    if(error) throw error;
    console.log('Renombrado');
  })

  <b>Sobreescribir archivo</b>
  fs.appendFile('data.txt', '\n Texto incluido con el método appendFile del módulo FS', (error) =>{
    if (error) console.log(`Error ${error}`)
  })

  <b>Eliminar archivo</b>
  fs.unlink('data2.txt', (error) =>{
    if(error) throw error;
    console.log('Eliminado')
  })

  <b>Leer archivo</b>
  fs.createReadStream('data.txt').pipe(fs.createWriteStream('data_new.txt'));

  <b>Leer folder</b>
  fs.readdir('../', (error, files) => {
    console.log('Root Folder \n')
    files.forEach(file => {
      if(file !== '.git' && file !== '.DS_Store' ) {
        console.log(file);
      }
    });
  });
</pre>

### NPM

Los paquetes son módulos que podemos establecer en un archivo de configuración. Estos módulos serán traidos desde NPM, que es el **Node Package Manager**.
La forma adecuada de manejar paquetes en NODEJS es por medio del archivo **package.json** el cual,
está conformado por toda la información disponible del proyecto y a su vez, por las **dependencias** que se utilizarán en el mismo.

### ¿Why uses NPM and set package.json FILE
The reason behind uses packages manager as gems in Rails, composer on Laravel or NPM on NodeJS, it because we can move our project to another computer or server, and we just need to run a command to install this modules on the computer we are using.

### Inicializar NPM 
<pre>
  <b>npm init</b>

  package name: (eps_08) <b>mc_code_project</b>
  version: <b>(1.0.0)</b>
  description: <b>NodeJS, Express y Mongoose.</b>
  entry point: <b>(index.js)</b> 
  test command: 
  git repository: <b>git@gitlab.com:williamromero/mc_code_tutorial.git</b>
  keywords: <b>course, nodejs</b>
  author: <b>William Romero</b>
  license: (ISC) <b>MIT</b>  
</pre>

Esto creará el archivo **package.json**, el cual funcionará como el colector de la configuración de nuestra aplicación y a su vez, detallará las **dependencias** a utilizar en el proyecto que estamos desarrollando.

### Instalar Módulo Lodash con NPM
<pre>
  npm i lodash --save
</pre>

Luego de correr el comando **npm install lodash --save** se creará la carpeta **node_modules** y el archivo **package.json** será sobrescrito y dentro del mismo, se creará un objeto nuevo el cual **indexará todas las dependencias** del proyecto. Pueden existir **dependencias globales (producción) y dependencias de desarrollo**. En este caso, estamos instalando una dependencia que funcionará en producción. Por lo tanto, si vemos el archivo **package.json** luego de correr el comando **npm install**, podremos apreciar que las mismas se han agregado o que si ya estaban agregadas, se han instalado dentro del folder **node_modules**.

<pre>
  <b>package.json</b>
  {
    "name": "mc_code_project",
    "version": "1.0.0",
    "description": "NodeJS, Express y Mongoose.",
    "main": "index.js",
    "scripts": {
      "test": "echo \"Error: no test specified\" && exit 1"
    },
    "repository": {
      "type": "git",
      "url": "git+ssh://git@gitlab.com/williamromero/mc_code_tutorial.git"
    },
    "keywords": [
      "course",
      "nodejs"
    ],
    "author": "William Romero",
    "license": "MIT",
    "bugs": {
      "url": "https://gitlab.com/williamromero/mc_code_tutorial/issues"
    },
    "homepage": "https://gitlab.com/williamromero/mc_code_tutorial#README",
    <b>
    "dependencies": {
      "lodash": "^4.17.5"
    }</b>
  }  
</pre>

To remove the file to our **node_modules** folder, we just need to write the next command:
<pre>
  sudo <b>npm uninstall lodash --save</b>
</pre>

If we have a project builded previously, we just need to run the command **npm install** to download the whole project dependencies to make it works.
<pre>
  npm install
</pre>

El módulo Lodash puede ser utilizado para manejar cadenas de objetos JSON o bien, manejar collections de data. Esto, para evitar usar **stringify** o **JSON.parse**.

<pre>
  const _ = require('lodash')
  let x = { "nombre" : "William"}
  let y = { "apodo" : "Chilli Willy"}

  let z = [ 
            { nombre:"William", apellido:"Romero", edad:"30"  }, 
            { nombre:"Lucia", apellido:"Castro", edad:"31"  } 
          ]

  let resultado = _.assign(x,y);
  console.log(resultado);
  console.log(`Lodash Assign Function: ${resultado.nombre}`);
  console.log(`Lodash Assign Function: ${resultado.apodo}`);

  let res_two = _.find(z, {nombre: "Lucia"});
  console.log(res_two);
  console.log(`Lodash Find Function: ${res_two.nombre} ${res_two.apellido} | Edad: ${res_two.edad}`)
</pre>

### Usando Lodash Argv para incluir data en la ejecución en consola

Para enviar datos o variables que podrían ser útiles en consola, utilizamos el **método ARGV**. Este, nos ayudará a crear un método o un parámetro, al cual le podremos agregar un valor. Así al correr el comando **node filename.js** podremos agregar 

<pre>
  <b>
    let comando = process.argv[2];
  </b>

  let x = { "nombre" : "William"}
  let y = { "apodo" : "Chilli_Willy"}

  if (comando === 'usuario' ){
    if (process.argv[3] === 'William') {
      console.log(_.assign(x,y))
    } else {
      console.log('Usuario no encontrado')
    }
  }  

  <b>node app.js usuario William</b>
  Respuesta: { nombre: 'William', apodo: 'Chilli_Willy' }
</pre>

### Usando YARGS para tomar datos la consola generando parámetros
Yargs, nos permite crear **parámetros** para construir búsquedas desde la ejecución de un archivo JS en consola. En el siguiente ejemplo, podremos apreciar como se crea el parámetro **usuario** el cual es testeado en el **if statement** cuando se corre el archivo con el comando **node app_two.js --usuario Lucia**.

<pre>
  <b>sudo npm i yargs --save</b>

  const _ = require('lodash')
  <b>const argvs = require('yargs').argv;</b>

  // console.log(argvs)

  let x = { "nombre" : "William"}
  let y = { "apodo" : "Chilli_Willy"}
  let z = [ 
            { nombre:"William", apellido:"Romero", edad:"30"  }, 
            { nombre:"Lucia", apellido:"Castro", edad:"31"  } 
          ]

  if (argvs.usuario === 'Lucia' ){
    let res_two = _.find(z, { nombre: argvs.usuario});
    console.log(res_two.nombre + ' ' + res_two.apellido);
  } else {
    console.log('Usuario no encontrado')
  } 
</pre>

Ahora corremos el app:
<pre>
  <b>node app_two.js --usuario Lucia</b>
    Lucia Castro
</pre>

[Más ejemplos de uso de Yargs](https://github.com/yargs/yargs/blob/master/docs/examples.md)

### DEBUGING INSPECTOR

Para evaluar el funcionamiento del código que hemos producido, podemos utilizar la herramienta **Inspect**, la cual nos ayudará a evaluar el comportamiento de nuestro código.

Creamos los archivos app.js y extra.js
<pre>
<b>app.js</b>
<b>
const extra = require('./extra')
extra.saludar();
</b>
let curso = "node";
curso = "node js";
curso = "node.js tutos";
debugger;
console.log(curso);

x = () => {
  return 1 + 1;
}
console.log(x());

<b>extra.js</b>
let mensaje = "Debuggin process"

function saludar(){
  console.log(mensaje);
}

module.exports = {
  saludar: saludar
}
</pre>

<pre>
  Iniciar el <b>inspector</b>

  node <b>inspect</b> app.js

  > 1 (function (exports, require, module, __filename, __dirname) { let curso = "node";
  2 curso = "node js";
  3 curso = "node.js tutos";

  Para correr entre lineas:
  debug &gt; <b>n</b>

  1 (function (exports, require, module, __filename, __dirname) { let curso = "node";
  &gt; 2 curso = "node js";
  3 curso = "node.js tutos";

  Para evaluar valores en variables:
  debug &gt; <b>repl</b>

  Press Ctrl + C to leave debug repl
  &gt; curso
  'node js'
  &gt; 

  Para ir al markdown debug o interrupción:
  debug &gt; <b>c</b>  

  break in app.js:4
    2 curso = "node js";
    3 curso = "node.js tutos";
  <b>&gt; 4 debugger;</b>
    5 console.log(curso);
    6 

  Y luego, para evaluar el estado de una variable:
  debug &gt; repl

  Press Ctrl + C to leave debug repl
  &gt; curso
  'node.js tutos'
  &gt;
</pre>

Si requerimos un módulo correría el debugger antes de correr el código del archivo que se está ejecutando.

<pre>
<b>
const extra = require('./extra')
extra.saludar();
</b>

let curso = "node";
curso = "node js";
curso = "node.js tutos";
debugger;
console.log(curso);

<b>En consola:</b>
  1 (function (exports, require, module, __filename, __dirname) { const extra = require('./extra')
  &gt; 2 extra.saludar();

  <b>break in extra.js:4</b>
    2 
    3 function saludar(){
  &gt; <b>4   debugger;</b>
    5   console.log(mensaje);
    6 }
</pre>

Para inspeccionar un archivo usando el explorador Chrome utilizamos el comando **--inspect-brk**, el cual abre un **inspector agent** y además, activa el puerto **(127.0.0.1:9229)** para que puedan hacerse cambios en el explorador en tiempo real.

Es oportuno tener en Chrome [**NIM (Node Inspector Manager)**](https://chrome.google.com/webstore/detail/nodejs-v8-inspector-manag/gnhhdgbaldcilmgcpfddgdbkhjohddkj/related) y también leer la [documentación](https://nodejs.org/en/docs/inspector/) para aprender sobre el funcionamiento del inspector de Node.

<pre>
  node --inspect-brk app.js
</pre>

Abre el explorador y podemos ir modificando los valores de las variables.

### Usando NODEMON para actualizar automáticamente los cambios

<pre>
  Para instalar el módulo <b>Nodemon</b>
  <b>npm i -g nodemon</b>

  Para ejecutarlo en un archivo
  <b>nodemon app.js</b>
  [nodemon] starting `node app.js`
  Debuggin process
  node.js tutos
  2

  <b>app.js</b>
  x = () => {
    return 1 + 10;
  }

  **** SALVAMOS ****

  [nodemon] starting `node app.js`
  Debuggin process
  node.js tutos
  11
</pre>

### Inspeccionar con Inspect & Nodemon

Lo que al usar el **inspect** de Node, teníamos que dirigirnos a la dirección [Chrome Inspect](chrome://inspect/#devices), ahora **Nodemon** nos permite abrirlo automáticamente.

<pre>
  <b>nodemon --inspect-brk app.js</b>
</pre>

### HTTP Requests

Para obtener datos de APIs podemos utilizar el módulo **Request**:

<pre>
  sudo npm init
  sudo npm i request --save
  sudo npm i yargs --save
</pre>

Y para realizar peticiones:

<pre>
<b>geocode.js</b>

const request = require('request');
const argv = require('yargs').argv;

let direccion = argv.direccion;
let url = `http://maps.googleapis.com/maps/api/geocode/json?address=${direccion}`

request({
  url: url,
  json: true
}, function (error, response, body){
  if(error) { console.log('Servicio no disponible')
  } else if (body.status == 'ZERO RESULTS'){ console.log('No hay datos que mostrar')
  } else if (body.status == 'OVER_QUERY_LIMIT'){ console.log('Límite excedido de consultas')
  } else if (body.status == 'OK'){  
    console.log(JSON.stringify(body, undefined, 2));
    console.log(JSON.stringify(body.results[0].formatted_address))
    console.log(JSON.stringify(body.results[0].address_components[1].long_name))
    console.log(JSON.stringify('Lat: '+body.results[0].geometry.location.lat))
    console.log(JSON.stringify('Lng: '+body.results[0].geometry.location.lng))  
  }
});
</pre>

Para encontrar una ubicación, usamos el comando dirección creado con Yargs y ejecutamos el archivo:
<pre>
  <b>node geocode.js --direccion="Tikal"</b>
  OR
  <b>nodemon --inspect-bkr geocode.js --direccion="Tikal"</b>
  {
    "results": [
      {
        "address_components": [
          {
            "long_name": "Tikal",
            "short_name": "Tikal",
            "types": [
              "establishment",
              "park",
              "point_of_interest"
            ]
          },
          {
            "long_name": "Tikal",
            "short_name": "Tikal",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "long_name": "Petén",
            "short_name": "Petén",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "Guatemala",
            "short_name": "GT",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "Tikal, Guatemala",
        "geometry": {
          "location": {
            "lat": 17.2220409,
            "lng": -89.6236995
          },
          "location_type": "GEOMETRIC_CENTER",
          "viewport": {
            "northeast": {
              "lat": 17.2233898802915,
              "lng": -89.6223505197085
            },
            "southwest": {
              "lat": 17.2206919197085,
              "lng": -89.62504848029151
            }
          }
        },
        "place_id": "ChIJc8ZiAsumX48RVRE-8jzDyd4",
        "types": [
          "establishment",
          "park",
          "point_of_interest"
        ]
      }
    ],
    "status": "OK"
  }
  "Tikal, Guatemala"
  "Tikal"
  "Lat: 17.2220409"
  "Lng: -89.6236995"
</pre>

### HTTP GITHUB API

Para trabajar con peticiones HTTP, descargamos el módulo **HTTPS** en nuestro proyecto:
<pre>
  <b>npm i https --save</b>
</pre>

Luego, debemos de configurar el NAVIGATOR_USER_AGENT que en este caso puede ser Chrome, Mozilla o IE. Para ello, declaramos algunas variables y trabajamos los request mediante el método **https.request**, el cual requerirá de dos parámetros que son **request** y **reponse**. Luego de ello, podremos maninupular la salida de datos y obtendremos lo que necesitamos del API que estamos consumiendo.

<pre>
  const https = require('https');
  const argv = require('yargs').argv;
  let username = argv.username

  let CHROME_USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36';
  <b>
  let options = {
    host: 'api.github.com',
    path: '/users/' + username,
    method: 'GET',
    headers: { 'user-agent': CHROME_USER_AGENT }
  };
  </b>
  let request = https.request(options, (response) => {
    let body = '';
    response.on('data', (out) => {
      body += out;
    });
  <b>
    response.on('end', (out) => {
      let json = JSON.parse(body);
      console.log(`Github Username: ${json.login}`);
      console.log('////////// USER DETAILS //////////');
      console.log(json)
    });
  </b>
  });

  request.on('error', (e) => {
    console.log(e);
  });

  request.end()
</pre>

Para ejecutarlo, podemos utilizar los siguientes comandos:
<pre>
<b> nodemon github.js --username=williamromero </b>
O
<b> node github.js --username=williamromero</b>

{ login: 'williamromero',
  id: 3052868,
  avatar_url: 'https://avatars3.githubusercontent.com/u/3052868?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/williamromero',
  html_url: 'https://github.com/williamromero',
  followers_url: 'https://api.github.com/users/williamromero/followers',
  following_url: 'https://api.github.com/users/williamromero/following{/other_user}',
  gists_url: 'https://api.github.com/users/williamromero/gists{/gist_id}',
  starred_url: 'https://api.github.com/users/williamromero/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/williamromero/subscriptions',
  organizations_url: 'https://api.github.com/users/williamromero/orgs',
  repos_url: 'https://api.github.com/users/williamromero/repos',
  events_url: 'https://api.github.com/users/williamromero/events{/privacy}',
  received_events_url: 'https://api.github.com/users/williamromero/received_events',
  type: 'User',
  site_admin: false,
  name: 'William Romero',
  company: null,
  blog: 'https://www.webres-studio.com',
  location: 'Guatemala, CA',
  email: null,
  hireable: true,
  bio: 'Full Stack Developer',
  public_repos: 10,
  public_gists: 5,
  followers: 1,
  following: 10,
  created_at: '2012-12-16T00:43:35Z',
  updated_at: '2018-02-14T15:10:12Z' 
}
</pre>

### PROMESAS
Las **promesas** son funciones que se utilizan para procesamientos asíncronos, las cuales cargan las respuestas cuando han cumplido con finalizar el proceso sin importar que la petición se haya hecho anteriormente.

<pre>
  <b>app_promises.js</b>
  let prom = require('./promises');
  prom.calcular(2, 3).then((resultado) => {
    console.log(`Resultado: ${resultado}`);
  }, (error)=> {
    console.log(error);
  }) 

  <b>promises.js</b>
  let calcular = (numero1,  numero2) => {
    return new Promise((res, rej)=> {
      setTimeout(() => {
        let suma = numero1 + numero2;
        if(suma > 5){
          res(numero1 + numero2);
        } else {
          rej('Error al procesar datos');
        }
      }, 2000);
    })
  }

  module.exports = {
    calcular : calcular
  }

  Para ejecutar este ejemplo:
  <b>nodemon app_promises.js</b>

  Luego de 2 segundos.
  <b>Error al procesar datos</b>

  Si cambiamos los valores y luego guardamos, obtendremos la siguiente respuesta luego de 2 segundos:

  >> prom.calcular(1,5) <<
  <b>Resultado: 6</b>
</pre> 

### FETCH API DATA
Proporciona una interfaz JS para manipular **HTTP pipes** como **requests y responses**.
Usualmente es utilizada con **promises** para operar de forma asíncrona data. [Firefox Doc](https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch)

Primero que nada, instalar el paquete en nuestro proyecto:
<pre>
  <b>sudo npm i node-fetch</b> 
</pre>

Luego la lógica del proyecto utilizará **Fetch** para requerir datos los cuales manejará de forma asíncrona mediante el uso de **promises**.

<pre>
  const fetch = require('node-fetch');

  let promesa = fetch('https://api.github.com/users/williamromero');
  promesa.then((res)=>{
    return res.json();
  }).then((json) =>{
    console.log(json.login);
  })

  let prom = fetch('https://www.webres-studio.com/api/v1/posts/5');
  prom.then((res)=>{
    return res.json();
  }).then((json) =>{
    console.log(json.data.title);
  })
  
  Para ejecutarlo:
  <b> node fetch.js </b>
</pre>

## EXPRESS HTTP FRAMEWORK

<pre>
const express = require('express');
const app = express();

let isLogin = () => true;

let logger = (req, res, next) => {
  console.log('REQ TYPE: ', req.method);
  next();
}

let showIP = (req, res, next) => {
  console.log('IP: ', req.headers['x-forwarded-for'] || req.connection.remoteAddress);
  next();
}

app.use((req, res, next)=>{
  if(isLogin()){
    next();
  } else {
    res.send('User is logged out')
  }
}, logger, showIP)

// app.use(logger);

app.get('/:user', (req, res) => {
  let usuario = req.params.user;
  res.send(`Hello ${req.method} ${usuario}`)
})

app.post('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.put('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.delete('/', (req, res) => {
  res.send(`Hello ${req.method} world`)
})

app.listen(3000, ()=> {
  console.log('Example app listening on port 9000!')
})
</pre>

### PUG TEMPLATE ENGINE

Para crear templates web, podemos utilizar **PUG** que es un motor de templates que convierte el código en HTML puro para exportar a web.

Para instalar **PUG** utilizamos el siguiente comando:
<pre>
  <b>npm i pug --save</b>
</pre>

Primero creamos el archivo **app.js**:
<pre>
const express = require('express')
const app = express();

let personas = [
  { id: 1, nombre: "William Romero" },
  { id: 2, nombre: "Oswaldo Vela" },
  { id: 3, nombre: "Luis Vega" }
]

<b>
Agregamos el módulo PUG mediante la siguiente sentencia para que reconozca el motor de templates.
app.set('view engine', 'pug')
</b>

<b>
Luego, en la vista se ejecuta el <b>res.render</b> en el cual se incluyen mediante la creación de un objeto, los elementos a transferir a la vista.
app.get('/', (req, res) => {
  res.render('template', {
    titulo: 'Pug Templating Engine',
    mensaje: 'Running Node/Express Webapp', 
    personas}
  )
})
</b>

app.listen(3000, () => {
  console.log('Running on port 3000')
})
</pre>

Luego, creamos el folder **VIEWS**, y dentro del mismo creamos el archivo **template.pug** en el cual añadiremos la visualización del contenido que hemos tomado del archivo **app.js**.

<pre>
html
  head
    title=titulo
    link(rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous")
  body

  div.container-fluid
    div.container
      div.col-md-9.row.justify-content-center

        h1=mensaje

        table.table.table-striped
          <b>Como PUG es un motor de templates, es posible que podamos incluir dentro de su sintaxis, un FOR de objetos para poder iterarlos</b>
          
          <b>
          for person in personas
            tr
              td= person.id
              td
                a(href='/persona/'+ person.id) #{person.nombre}
          </b>
</pre>

### EXPRESS-GENERATOR
Es un módulo utilizado para crear un esqueleto de aplicación rápidamente. Para poder utilizarlo en cualquier proyecto, lo instalamos de forma global.
<pre>
  npm install express-generator -g
</pre>

Para crear el proyecto con estructura y motor de templates, ingresamos el siguiente comando en consola:
<pre>
  express --view=pug pug-generator
</pre>

Luego, nos redirigimos al folder **pug-generator** y ejecutamos los siguientes comandos. Para correrlo la primera vez, hay que instalar los módulos que trae inicialmente:
<pre>
  npm install
</pre>

Y para correrla, utilizamos el siguiente comando:
<pre>
  npm start
</pre>

Ahora para ver lo que hemos hecho en el anterior proyecto, debemos de agregar al **index** event, vamos al **folder ROUTES** para agregar la lógica de lo que se enviará mediante la ruta y luego agregar el código que procesará el objeto:

<pre>
  cd routes
  nano index.js

  var express = require('express');
  var router = express.Router();

  /* GET home page. */
  let personas = [
    { id: 1, nombre: "William Romero" },
    { id: 2, nombre: "Oswaldo Vela" },
    { id: 3, nombre: "Luis Vega" }
  ]

  <b>##Aquí cambiamos "app" por "router" ya que lo hemos requerido arriba</b>
  <b>router.get</b>('/', (req, res) => {
  ##Aquí cambiamos "template" por "index" ya que ese es su nombre, en las vistas
    res.render(<b>'index'</b>, {
      titulo: 'Pug Templating Engine',
      mensaje: 'Running Node/Express Webapp', 
      personas}
    )
  })

  module.exports = router;
</pre>

Luego en el template agregamos el código de ejecución tanto en **layout.pug** para agreagar **Bootstrap** y luego, vamos al indice para colocar la programación antes utilizada para iterar el objeto Personas:

<pre>
  <b>views/index.pug</b>
  extends layout

  block pug_data
  div.container-fluid
    div.container
      div.col-md-9.row.justify-content-center
        h1=mensaje
        table.table.table-striped
          for person in personas
            tr
              td= person.id
              td
                a(href='/persona/'+ person.id) #{person.nombre}  

  <b>views/layout.pug</b>
  doctype html
  html
    head
      title=titulo
      link(rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous")
    body
      block pug_data
</pre>

## MONGO DB
Es una DB no relacional. Para instalar, ver este [tutorial](https://www.youtube.com/watch?v=KGZj_UD7amU&index=19&list=PLvimn1Ins-41lVr-SPWF1mdNTzog05TcA).

Para iniciar el servidor de DBs, utilizamos el siguiente comando:
<pre>
<b>
mongod
</b>

Y recibiremos la siguiente indicación: 
<b>
  2018-02-27T01:49:02.581-0600 I NETWORK  [initandlisten] waiting for connections on port 27017
</b>
</pre>

Luego también, para iniciar a manipular la DB, podemos ejecutar el siguiente comando:
<pre>
  mongo
</pre>

#### Para crear datos:
Las siguientes opciones, funcionan de manera diferente. Mientras que la primera solo crea el nombre o registro de la DB que deseamos crear, la segunda, crea directamente una DB con el nombre personas.

<pre>
  use <b>database_name</b>
  O
  <b>db.Personas.insert({id: 1, nombre:"William Romero", mensaje:"Prueba de contenido"})</b>
</pre>

#### Para buscar contenidos:
<pre>
  > <b>db.Personas.find()</b>
    { "_id" : ObjectId("5a950da67ed470a5c14ca57c"), "id" : 1, "nombre" : "William Romero", "mensaje" : "Prueba de contenido" }
    { "_id" : ObjectId("5a950e8a7ed470a5c14ca57d"), "id" : 2, "nombre" : "Allan Romero", "mensaje" : "Prueba de contenido" }
</pre>

## MONGOOSE (CRUD)

Ver proyecto en el [Github](https://github.com/mitocode21/node-crud)

Mongoose es un ORM, quiere decir que se encargará directamente de la creación de collections y de sus respectivos archivos.

<pre>
  npm install mongoose --save
</pre>

Dentro del folder creamos el folder **config** y dentro del mismo, el archivo **conexion.js**:
<pre>
  let mongoose = require('mongoose');
  mongoose.connect('mongodb://localhost:27017/crud', {useMongoClient: true});

  module.exports = mongoose;
</pre>

En este poco de código, vamos a requerir el módulo **Mongoose** para que al momento de generar el modelo, este pueda ingresar a la DB local y pueda adherirse al proyecto.

Siguiente a ello, creamos el folder **models** en el cual vamos a crear el archivo **persona.js** que básicamente fungirá como el **schema** que **Mongoose** tomará para realizar las integraciones con la DB no relacional.

<pre>
  let mongoose = require('mongoose');
  let Schema = mongoose.Schema;

  let personaSchema = new Schema({
    id: {type: String},
    nombres: {type: String},
    apellidos: {type: String},
    edad: {type: Number, min: 0 }
  }, {versionKey: false});

  // Variable de Instancia del Objeto Persona
  let Persona = mongoose.model('Personas', personaSchema);
  module.exports = Persona;
</pre>


