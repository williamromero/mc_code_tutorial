const express = require('express')
const app = express();

let personas = [
  { id: 1, nombre: "William Romero" },
  { id: 2, nombre: "Oswaldo Vela" },
  { id: 3, nombre: "Luis Vega" }
]

app.set('view engine', 'pug')

app.get('/', (req, res) => {
  res.render('template', {
    titulo: 'Pug Templating Engine',
    mensaje: 'Running Node/Express Webapp', 
    personas}
  )
})

app.listen(3000, () => {
  console.log('Running on port 3000')
})
